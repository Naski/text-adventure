package com.company;

import java.util.*;

/*A level needs to be defined as following:
 * The constructed description is the notVisited description you get when the room is normally unlocked
 * A description needs to be added for every state which are the following:
 * "Wall" (gets set automatically when the Level is generated as a wall level)
 * "notVisited" (Thats the default description given by the constructor)
 * "locked" (Thats the description for eg. a door which you can´t pass by before using a key
 * "visited" (Thats the description for when the player visits a room 2 or more times. That way it becomes more dynamical)
 * "lockedCheckpoint" (Thats the desctiption for a room that is former not visitable but activatable by a checkpoint. eg. FIL7 to FloorSub
 *
 *Adjacent Levels do always have to be generated either through the method
 *  addImpassables() to give them a default name (rather debug) or through
 *  addImpassablesDescribed() to give every side of the room a different Description, which doesn´t change so be careful
 *
 * Items have to be defined in the room in order to be actually pickable
 * If a room has eg a door that is locked the usable
 * */

public class GameWorld {

    String cellUnvisited =  "\nMILAAA! Wo bin Ich? Als ich versuche aufzustehen, merke ich wie schwach mein Körper ist. " +
                            "Mein Schädel brummt und ich zitter am ganzen Körper. Es ist dunkel, kalt und feucht. " +
                            "Ich bin in einer Zelle. Die Tür ist geöffnet. - Eine Falle?";

    String cellVisited = "\nHier in der Zelle scheint kein Ausweg zu sein. Ich muss mir einen anderen Weg suchen.";

    String cellEast = "\nEine Kiste mit einem Loch in der Mitte. Es stinkt abartig. Ich kann kaum atmen wegen dieser ”Toilette”. ";

    String cellSouth = "\nFenstergitter. Zu hoch um dran zu kommen.";

    String cellWest = "\nEin Bett. Eher gesagt eine lange Platte aus Metal. Keine Decke, kein Kissen. Garnichts.";

    String floorMainUnvisited = "\nOK. Hier scheint niemand zu sein. Wie kann es sein? Was ist passiert?" +
            " Meine Kleider hängen nass und schwer an meinem Körper herunter.";

    String floorMainVisited = "\nWieder im Flur. In welche Richtung sollte ich am besten gehen.";

    String floorMainWest = "\nAbgeschlossen";

    String labUnvisited = "\nEin Seziertisch. Embryos in Glass. Messgeräte. Scheint wohl ein Labor zu sein." +
            " Ich muss schnell Mila finden. Wer weiss, was man mit ihr angestellt hat. ";

    String labVisited = "\nDas Labor. Mir gefällt dieser Raum überhaupt nicht.";

    String labNorth = "\nEin Regal. Vielleicht finde ich hier etwas nützliches. \n" +
            "Kisten mit Ordnern. Reagenzgläser. Schutzbrille. Ein Remote-Button?\n" +
            "Bücher. Handschuhe.";

    String bathroomUnvisited = "\nFuck! Meine Schuhe quietschen bei jedem Schritt. - Ich muss aufpassen.\n" +
            "Rundum Fliesen. Es scheint ein Waschraum zu sein. \n" +
            "Wo zum Teufel sind wir hier gelandet?";

    String bathroomVisited = "\nWieder der Waschraum. Was hier wohl alles schon passiert ist.";

    String bathroomNorth = "\nÜberall sind Blutflecken";

    String bathroomSouth = "\nSind das Innereien? Ich hoffe nicht von einem Menschen.";

    String escapeUnvisited = "\nWir schaffen es aus dem Fenster zu klettern. Gleichzeitig laufen " +
            "Tränen der Erleichterung über unsere Wangen.\\nEs wurde bereits Hell draussen. \\n\" +\n" +
            "                \"Majestätisch gewachsene Bäume spendeten einen erholsamen Schatten.\\n\" +\n" +
            "                \"Man hatte uns entführt und fast umgebracht. \\n\" +\n" +
            "                \"Wir streiften noch einmal die Hölle. \\n\" +\n" +
            "                \"\\n\" +\n" +
            "                \"Während wir mit letzter Kraft zum nächsten Dorf flohen, liefen wir an einer Person vorbei, \" +\n" +
            "                \"die an einem Baum stand. Er trug einen schwarzen Regenmantel und hob seinen Kopf. \" +\n" +
            "                \"Ich sah wie seine Augen spöttisch blitzten. \\n\" +\n" +
            "                \"Noch fester hielt ich ihre Hand und unsere Schritte wurden immer schneller.\\n\";";

    String escapeLocked = "\nEin Fenster! Ich sehe nur Bäume. Es scheint ein abgelegener Ort zu sein. \n" +
            "Verdammt. Es ist verschlossen. Vielleicht kann ich es später mit einem harten Gegenstand öffnen. ";

    String lockerUnvisited = "\nLeise versuche ich die nächste Tür zu öffnen. Hier sieht es aus wie in einem Depot. " +
            "Vielleicht finde ich hier etwas nützliches.";

    String lockerVisited = "\nIn einem Depot muss doch etwas sein?!";

    String lockerEast = "\nNichts außer Akten.";

    String lockerSouth = "\nWieder irgendein Labor Scheiß. Ich werde verrückt.";

    String lockerWest = "\nAha. Hiervon könnte vielleicht etwas nützlich sein. leere Spritzen, Verband, eine Atemmaske. Kabel.";

    String floorChoiceUnvisited = "\nNun bin ich drin. Ein langer Flur. Keine Fenster. KerzenLicht. " +
            "In jeder Richtung eine Tür. - Ein Raum wie aus einem Horrorfilm.\n";

    String floorChoiceVisited = "\nOK. Was sich wohl hinter der nächsten Tür befindet?!";

    String floorChoiceLocked = "\nDiese Tür scheint verschlossen zu sein. Mila. Halte Durch!";

    String storageUnvisited = "\nEin Lager. Ich muss konzentriert bleiben!";

    String storageVisited = "\nVielleicht habe ich etwas wichtiges übersehen?!";

    String storageNorth = "\nEin großer Raum, in dem viele Sachen drin stehen. Es muss doch was nützliches dazwischen sein. " +
            " OMG - Eine Bombe!";

    String storageEast = "\nAuch dieser Raum scheint extra verstärkt zu sein.";

    String storageSouth = "\nEin Schlüssel! ";

    String killerDogUnvisited = "\nVerdammt. Diese armen Wesen. Wer ist so krank, das er solche Kreaturen erschafft. " +
            "Ich hatte keine andere Wahl. Der plötzliche Tod ist besser als in dieser Hölle.";

    String killerDogVisited = "\nWieder in dem Wolfsraum.";

    String killerDogNorth = "\nDeshalb waren hier wohl soviele Kreaturen. Ein solches Schwert habe ich noch nie in meinem Leben gesehen. " +
            "Das kann ich sicherlich gebrauchen.";

    String killerDogEast = "\nDer ganze Raum ist in Blut getränkt. ";

    String killerDogWest = "\nViel ist hier nicht. Ein dunkle Kammer. Selbst für diese Kreaturen eine Qual.";

    String killerDogLocked = "\nHinter dieser Tür ist ein ganzes Rudel genveränderter Wölfe.\n";

    String FIL1Locked = "\nDiese Tür ist ebenfalls verschlossen."; //Es gibt nur einen Floor is Lava Raum, welcher vorher geschlossen ist. Eine Art Tür

    String FILUnvisited = "\nIch muss auf jeden Schritt achten. Dieser war schonmal richtig.";

    String FILVisited = "\nNein. Bitte nicht durch diesen Raum.";

    String FILDead = "\nDu bist in ein tiefes schwarzes Loch gefallen."; //Alle Räume, in denen der Boden Lava ist, bekommen diese Beschreibung

    String floorSubUnvisited = "\nPuh. Ich habe es aus diesem kranken Raum heraus geschafft." +
            " Von Adrenalin durchtränkt scheinen meine Sinne geschärft zu sein.\n" +
            "Plötzlich höre ich ganz leise ein Rufen. Es ist Mila!";

    String floorSubVisited = "\nim Gang";

    String floorSubLocked = "\nNoch kann ich nicht durch diese Tür."; //Verbindung zwischen FloorSub und FloorMain, eine Art Tür

    String gasUnvisited = "\nDer Raum ist mit Gas gefüllt.";

    String gasVisited = "\nWeiter zu Mila.";

    String gasNorth = "\nein Bett.";

    String gasEast = "\nWaschbecken. Toilette. Abgesehen vom Gas scheint das ein ganz normaler Raum zu sein.";

    String gasWest = "\nEin Werkzeugkasten! Das kann ich gebrauchen. Was. Nur ein Hammer." +
            "Das sollte aber ausreichen.";

    String gasLocked = "\nEin Raum gefüllt mit Giftgas.";

    String princessUnvisited = "\nWir fallen uns sofort in die Arme. Nie wieder werde Ich Dich loslassen.";

    String princessVisited = "\nNichts wie weg hier.";

    String princessNorth = "\nLos! Wir müssen hier verschwinden.";

    String princessWest = "\nKannst du laufen? Ich nimm Dich in meine Arme.";

    String princessLocked = "\nVor mich stellt sich plötzlich ein dreiköpfiges Monster. " +
            "Es trägt ein Beil in seiner Hand. \n" +
            "Mila ruft, das ich schnell verschwinden soll, doch dafür ist es bereits zu spät. " +
            "Ich werde Dich retten. Ich strecke das Schwert und laufe brüllend auf das Monster zu.";


    Level Cell = new Level(cellUnvisited); // North Floor
    Level FloorMain = new Level(floorMainUnvisited); // South Cell , East Lab
    Level Lab = new Level(labUnvisited); //West Floor
    Level Bathroom = new Level(bathroomUnvisited);
    Level Escape = new Level(escapeUnvisited);
    Level Locker = new Level(lockerUnvisited);
    Level FloorChoice = new Level(floorChoiceUnvisited); //North
    Level Storage = new Level(storageUnvisited);
    Level KillerDog = new Level(killerDogUnvisited);
    Level FIL1 = new Level(FILUnvisited);
    Level FIL2 = new Level(FILUnvisited);
    Level FIL3 = new Level(FILUnvisited);
    Level FIL4 = new Level(FILUnvisited);
    Level FIL5 = new Level(FILUnvisited);
    Level FIL6 = new Level(FILUnvisited);
    Level FIL7 = new Level(FILUnvisited);
    Level FloorSub = new Level(floorSubUnvisited);
    Level Gas = new Level(gasUnvisited);
    Level Princess = new Level(princessUnvisited);
    Item sword = new Item("\uD83D\uDDE1", "sword", "key");
    Item bomb = new Item("\uD83D\uDCA3", "bomb", "key");
    Item key = new Item("\uD83D\uDDDD", "key", "key");
    Item mask = new Item("\uD83E\uDD3F", "mask", "key");
    Item hammer = new Item("\uD83D\uDD28", "hammer", "key");
    Item princess = new Item("\uD83D\uDC78", "princess", "key");
    Item button = new Item("\uD83D\uDECE", "button", "press");

    public GameWorld() {
        buildConnections();
        setStatus();
        addImpassables();
        addDescriptions();
        addUsableItems();
        addSurvivableItem();
        addItemsToLevels();
        FIL7.setUnlocksCheckpoint(true);
        KillerDog.setDragsLive(true);
        Gas.setDragsLive(true);
        Princess.setDragsLive(true);
    }

    public void buildConnections() {
        addAdjacentLevel(Cell, FloorMain, "n");
        addAdjacentLevel(FloorMain, Lab, "e");
        addAdjacentLevel(FloorMain, FloorChoice, "n"); //Impassable
        FloorSub.addAdjacentLevel(FloorMain, "s");
        addAdjacentLevel(Lab, Bathroom, "e");
        addAdjacentLevel(Lab, Locker, "s");
        addAdjacentLevel(Bathroom, Escape, "e"); //Impassable
        addAdjacentLevel(FloorChoice, Storage, "e");
        addAdjacentLevel(FloorChoice, KillerDog, "n");
        addAdjacentLevel(FloorChoice, FIL1, "w"); //Impassable
        addAdjacentLevel(FIL1, FIL2, "w");
        addAdjacentLevel(FIL2, FIL3, "n");
        addAdjacentLevel(FIL3, FIL4, "n");
        addAdjacentLevel(FIL4, FIL5, "w");
        addAdjacentLevel(FIL5, FIL6, "w");
        addAdjacentLevel(FIL6, FIL7, "s");
        addAdjacentLevel(FIL7, FloorSub, "w"); //Warning impassable
        addAdjacentLevel(FloorSub, Gas, "n");
        addAdjacentLevel(FloorSub, Princess, "w");
    }

    public void setStatus() {
        FloorSub.setStatus("unvisited");
        FloorChoice.setStatus("locked");
        Escape.setStatus("locked");
        FIL1.setStatus("locked");
        KillerDog.setStatus("locked");
        Gas.setStatus("locked");
        Princess.setStatus("locked");
    }

    public void addDescriptions() {
        FloorChoice.addDescription("locked", floorChoiceLocked);
        Escape.addDescription("locked", escapeLocked);
        FIL1.addDescription("locked", FIL1Locked);
        FloorSub.addDescription("lockedCheckpoint", floorSubLocked);
        Princess.addDescription("locked", princessLocked);
        Gas.addDescription("locked", gasLocked);
        KillerDog.addDescription("locked", killerDogLocked);


        Cell.addDescription("visited", cellVisited);
        FloorMain.addDescription("visited", floorMainVisited);
        Lab.addDescription("visited", labVisited);
        Bathroom.addDescription("visited", bathroomVisited);
        //Escape has no visited case
        Locker.addDescription("visited", lockerVisited);
        FloorChoice.addDescription("visited", floorChoiceVisited);
        Storage.addDescription("visited", storageVisited);
        KillerDog.addDescription("visited", killerDogVisited);
        FIL1.addDescription("visited", FILVisited);
        FIL2.addDescription("visited", FILVisited);
        FIL3.addDescription("visited", FILVisited);
        FIL4.addDescription("visited", FILVisited);
        FIL5.addDescription("visited", FILVisited);
        FIL6.addDescription("visited", FILVisited);
        FIL7.addDescription("visited", FILVisited);
        FloorSub.addDescription("visited", floorSubVisited);
        Gas.addDescription("visited", gasVisited);
        Princess.addDescription("visited", princessVisited);
    }

    public void addImpassables() {
        Cell.addImpassablesDescribed("", cellEast, cellSouth, cellWest);
        FloorMain.addImpassablesDescribed("", "", "", floorMainWest);
        Lab.addImpassablesDescribed(labNorth,"","","");
        Bathroom.addImpassablesDescribed(bathroomNorth, "", bathroomSouth, "");
        Escape.addImpassables();
        Locker.addImpassablesDescribed("", lockerEast, lockerSouth, lockerWest);
        FloorChoice.addImpassables();
        Storage.addImpassablesDescribed(storageNorth, storageEast, storageSouth, "");
        KillerDog.addImpassablesDescribed(killerDogNorth, killerDogEast, "", killerDogWest);
        FIL1.addImpassablesDeadDescribed(FILDead, "", FILDead, FILDead);
        FIL2.addImpassablesDeadDescribed("", "", FILDead, FILDead);
        FIL3.addImpassablesDeadDescribed("", FILDead, "", FILDead);
        FIL4.addImpassablesDeadDescribed(FILDead, FILDead, "", "");
        FIL5.addImpassablesDeadDescribed(FILDead, "", FILDead, "");
        FIL6.addImpassablesDeadDescribed(FILDead, "", "", FILDead);
        FIL7.addImpassablesDeadDescribed("", FILDead, FILDead, "");
        FloorSub.addImpassables();
        Gas.addImpassablesDescribed(gasNorth, gasEast, "", gasWest);
        Princess.addImpassablesDescribed(princessNorth, "", "", princessWest);
    }

    public void addItemsToLevels() {
        Lab.addItemToMapInventory(button);
        Locker.addItemToMapInventory(mask);
        Storage.addItemToMapInventory(key);
        Storage.addItemToMapInventory(bomb);
        KillerDog.addItemToMapInventory(sword);
        Gas.addItemToMapInventory(hammer);
        Princess.addItemToMapInventory(princess);
    }

    public void addUsableItems() {
        FloorChoice.addUsableItem(button);
        Escape.addUsableItem(hammer);
        KillerDog.addUsableItem(bomb);
        Princess.addUsableItem(sword);
        Gas.addUsableItem(mask);
        FIL1.addUsableItem(key);
    }

    public void addSurvivableItem() {
        Princess.addSurvivableItem(sword);
        Gas.addSurvivableItem(mask);
        KillerDog.addSurvivableItem(bomb);
    }

    public void addAdjacentLevel(Level lvl1, Level lvl2, String direction) {
        lvl1.addAdjacentLevel(lvl2, direction);
        lvl2.addAdjacentLevel(lvl1, lvl1.getOppositeDirection(direction));
    }

    public Level getStartLevel() {
        return Cell;
    }
}