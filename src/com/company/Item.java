package com.company;

import java.util.ArrayList;
import java.util.List;

public class Item {
    String emoji;
    String keyValue;
    String usageType;
    public Item(String emoji, String keyValue, String usageType) {
        this.emoji = emoji;
        this.keyValue = keyValue;
        this.usageType = usageType;
    }

    public String getEmoji() {
        return emoji;
    }
    public String getUsageType() {
        return  usageType;
    }
    public String getKeyValue() {
        return keyValue;
    }
}
