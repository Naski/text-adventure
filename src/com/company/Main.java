package com.company;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static Commands cmds = new Commands();
    public static void main(String[] args) {
        //String deathMessage = "Versuchh es erneut";
        //String winMessage = "";
        String welcomeMessage = "█                               The Escape                                 █";
        String Intro = "\nDie Dunkelheit der Nacht schluckte die schweren Tropfen des Regens " +
                "\nwie ein hungriges Tier. Nur hier und da erleuchtete eine Laterne den Gehweg, " +
                "\nder sich schwarz um die Häuser schlingerte. Niemand war unterwegs. Niemand außer Wir.\n" +
                "\nWegen einer Lappalie haben wir uns gestritten und uns so sehr verrannt, " +
                "\ndas wir nicht mehr in der Lage waren aufeinander aufzupassen.  \n" +
                "\n" +
                "Wir trennten uns. Sie entfernte sich von mir und Ich hielt sie nicht zurück. \n" +
                "\n" +
                "Gefangen in meinem Trotz lief ich nun alleine weiter. " +
                "\nTiefgefroren, unfähig den Weg nach Hause zu finden. \n" +
                "\n" +
                "Plötzlich. Ein Aufschreien. Es ist ihre Stimme. Doch die Art von Schrei, " +
                "\nwie wenn eine Mutter ihr Kind verliert. \n" +
                "\n" +
                "Ich rannte los! Mein Herz pochte! Schweiß trieb aus all meinen Poren und " +
                "\nvermischte sich auf meinem Gesicht mit dem Regenwasser. \n" +
                "\n" +
                "Ich rief und lief so stark ich konnte. MILA! MILA! MILA! \n" +
                "\n" +
                "Ein Schatten löste sich an meiner Seite & nun ist es DUNKEL.\n";

        Scanner sc = new Scanner(System.in);
        boolean playing = true;
        boolean win = false;
        Output out = new Output();
        GameWorld world = new GameWorld();
        Level currentLevel = world.getStartLevel();
        Level lastLevel = currentLevel;
        User usr = new User();
        out.addHeader(welcomeMessage);
        out.addHeader(Intro);
        out.addBody("Drücke beliebige Taste zum fortfahren....");
        out.printToPlayer();
        sc.nextLine();

        while(playing) {
            //When starting the game loop, first the list of available commands and the inventory is printed to the console
            out.addHeader(cmds.printCmds());
            out.addHeader(usr.printLivesAndInventory());
            out.addBody(currentLevel.getCurrentDescription());

            if(!currentLevel.isPassable() && currentLevel.status.equals("wall")) {
                currentLevel = lastLevel;
            }
            if(currentLevel.dragsLive()) {
                if(currentLevel.hasSurvivableItem()) {
                    if(usr.isItemContainedInInventoryByEmoji(currentLevel.getSurvivableItem().emoji)) {
                      out.addBody("\nBenutze schnell " + currentLevel.getSurvivableItem().emoji);
                    } else {
                        if (usr.hurtAndIsDead()) {
                            System.out.println("Du bist Tod");
                            break;
                        } else {
                            out.addBody("Das war eine schlechte Idee. Ich brauche ein Item");
                            currentLevel = lastLevel;
                        }
                    }
                } else{
                    if (usr.hurtAndIsDead()) {
                        System.out.println("Du bist Tod");
                        break;
                    } else {
                        out.addBody("Das war eine schlechte Idee");
                        currentLevel = lastLevel;
                    }
                }

            }
            out.printToPlayer();

            String uInput = sc.nextLine();
            uInput = uInput.toLowerCase();
            if(isValidInput(parseInput(uInput))) {
                switch(parseInput(uInput)[0]) {
                    case "n" : { }
                    case "e" : { }
                    case "s" : { }
                    case "w" : {
                        if(!currentLevel.isPassable()) {
                            currentLevel = lastLevel;
                        }
                        out.addBody("Ich gehe nach und blicke nach "+cmds.printCommand(uInput));
                        Level nextLevel = currentLevel.getLevelInDirection(uInput);
                        lastLevel = currentLevel;
                        currentLevel = nextLevel;
                        break;
                    }

                    case "t" : {
                        String uItemName = parseInput(uInput)[1];
                        String emoji = cmds.parseInputToEmoji(uItemName);
                        if(currentLevel.containsItemByEmoji(emoji)) {
                            out.addBody("Ich nehme das "+emoji+" auf");
                            usr.addToInventory(currentLevel.getItemByEmoji(emoji));
                            currentLevel.removeItemByEmoji(emoji);
                        }
                        else {
                            out.addBody("Hier ist kein "+uItemName+" was ich aufheben könnte");
                        }
                        break;
                    }
                    case "u" : {
                        /* When using:
                        Can I use an item in this room?
                        Is the item I can use in this room the item I used?
                         */
                        String uItemName = parseInput(uInput)[1];
                        String emoji = cmds.parseInputToEmoji(uItemName);
                        if(usr.isItemContainedInInventoryByEmoji(emoji)) {
                            if(currentLevel.hasUsable()) {
                                String usageType = usr.getItemFromInventoryByEmoji(emoji).getUsageType();
                                out.addBody("Du hast den " + usageType + usr.getItemFromInventoryByEmoji(emoji).emoji+ " benutzt.");
                                if (currentLevel.isKeyValid(usr.getItemFromInventoryByEmoji(emoji))) {
                                    currentLevel.setStatus("unvisited");
                                    currentLevel.setDragsLive(false);
                                    usr.dropFromInventoryByEmoji(emoji);
                                } else {
                                    out.addBody("Warum soll ich hier " +uItemName+ " benutzen. Das macht keinen Sinn");
                                }
                            }
                            else {
                                out.addBody("Warum soll ich hier \" +uItemName+ \" benutzen. Das macht keinen Sinn");
                            }
                        }
                        else {
                            out.addBody("Ich habe kein "+uItemName+" was ich benutzen kann");
                        }
                        break;
                    }
                    case "d" : {
                        String uItemName = parseInput(uInput)[1];
                        String emoji = cmds.parseInputToEmoji(uItemName);
                        if(!usr.isItemContainedInInventoryByEmoji(emoji)) {
                            out.addBody("Ich hab kein "+uItemName+" zum wegwerfen");
                        } else {
                            currentLevel.addItemToMapInventory(usr.getItemFromInventoryByEmoji(emoji));
                            usr.dropFromInventoryByEmoji(emoji);
                            out.addBody("Weg damit. Ich schmeiß das *** " + emoji + " in den Raum");
                            break;
                        }
                    }
                }

            }
        }
    }
    //Uses the List of valid Inputs from the Commands class to determine if the user used a valid command
    //Input: String | Output: Boolean
    //Usage: isValidInput("n") => true isValidInput("Gulasch") => false
    public static String[] parseInput(String uInput) {
        String[] result = uInput.split(" ");
        return result;
    }
    public static boolean isValidInput(String[] input) {
        for(String obj : cmds.getCmds()) {
            if(input[0].equals(obj)) {
                return true;
            }
        }
        return false;
    }
}
