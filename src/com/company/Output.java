package com.company;

import java.util.ArrayList;
import java.util.List;

public class Output {
    //Ready to Output texts are saved in the lists
    List<String> header = new ArrayList<>();
    List<String> body = new ArrayList<>();
    List<String> debug = new ArrayList<>();
    int rows = 12;

    public Output() {
        buildBody();
    }
    public void addBody(String text) {
        body.add(text);
    }
    public void addHeader(String text) {
        header.add(text);
    }
    public void addDebug(String text) {
        debug.add(text);
    }
    public void printToPlayer() {
        String header = buildHeader();
        int totalLines = header.split("\n").length;
        String body = buildBody();
        totalLines += body.split("\n").length;
        System.out.print(header);
        System.out.print(body);
        while(totalLines<12) {
            System.out.println(" ");
            totalLines++;
        }
        clearOutputs();

    }
    private String buildHeader() {
        String result = printBar();
        for(String obj : header) {
            result+=obj+"\n"+printBar();
        }
        return result;
    }

    public String printBar() {
        String result = "";
        for(int i=0; i<header.get(0).length();i++) {
            result+="█";
        }
        return result+="\n";
    }
    //Measures if the sentence is longer than 30 letters. Adds a Line Break there
    private String buildBody() {
        String result = "";
        int lineLength = 0;
        for(String obj : body) {
            String[] sentence = obj.split(" ");
            int sentenceLength = header.get(0).length();
            for(String word : sentence) {
                lineLength+=word.length();
                if(lineLength < sentenceLength) {
                    result+=word+" ";
                } else {
                    lineLength = 0;
                    result+="\n "+word+" ";
                }
            }
            result+="\n";
        }
        return result;
    }
    private String buildDebug() {
        String result = "";
        for(String obj: header) {
            result+=obj+"\n";
        }
        return result;
    }

    private void clearOutputs() {
        header.clear();
        body.clear();
        debug.clear();
    }
    public void cancelOutput() {
        clearOutputs();
    }
}
