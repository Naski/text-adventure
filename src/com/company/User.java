package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
    List<Item> inventory = new ArrayList<Item>();
    String name;
    int lives = 4; //Also 3 Fehlversuche
    public User() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public String printLivesAndInventory() {
        String result = "█ Leben: ";
        for (int i=1;i<lives;i++) {
            //System.out.println("Drawing "+i+".th heart");
            result+="♡";
        }
        result+=" █";
        if(countInventory() == 0) {
            result+= " Du besitzt keine Items";
        }
        else {
            result+="█ Inventory █ ";
            for (Item obj : inventory) {
                result += obj.emoji + " █";
            }
        }
        return result;
    }
    public int countInventory() {
        int result = 0;
        for(Item obj : inventory) {
            result++;
        }
        return result;
    }
    public void addToInventory(Item i) {
        inventory.add(i);
    }
    public Item getItemFromInventoryByEmoji(String emoji) {
        Item result = new Item("Debug", "debug", "debug");
        for(Item obj : inventory) {
            if(obj.emoji.equals(emoji)) {
                result = obj;
            }
        }
        return result;
    }
    public void dropFromInventoryByEmoji(String emoji) {
        for(int i=0;i<inventory.size();i++) {
            if(inventory.get(i).emoji.equals(emoji)) {
                inventory.remove(i);
            }
        }
    }
    public boolean isItemContainedInInventoryByEmoji(String emoji) {
        boolean result = false;
        for(Item obj : inventory) {
            if(obj.emoji.equals(emoji)) {
                result = true;
            }
        }
        return result;
    }
    public boolean hurtAndIsDead() {
        boolean result = false;
        lives--;
        if(lives<=0) {
            result = true;
        }
        return result;
    }
}